package gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import entities.Weather;

@Slf4j
public class YandexWeatherGateway {
    //для корректнои работы необходимо было :
    // 1. изменить "https://api.weather.yandex.ru/v1/informers" на " https://api.weather.yandex.ru/v1/forecast";
    // 2. добавить TOKEN = "e02bcd5e-566d-4c42-a109-79a2ddcebd40" - его я удалю 05.07.2021
    // 3. в YandexWeatherTest необходимо изменить MOSCOW_LATITUDE = "56.630842"; MOSCOW_LONGITUDE = "47.886089";

    private static final String URL = "https://api.weather.yandex.ru/v1/forecast";
  //private static final String URL = "https://api.weather.yandex.ru/v1/informers";
  private static final String TOKEN = "e02bcd5e-566d-4c42-a109-79a2ddcebd40";


    @SneakyThrows
    public Weather getWeather(String translated_text, String longitude, String language) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.get(URL)
                .header("Accept", "*/*")
                .header("X-Yandex-API-Key", TOKEN)
                .queryString("text", translated_text)
                .queryString("lang", longitude)
                .queryString("lang", language)
                .asString();
        String strResponse = response.getBody();
       log.info("response: "+strResponse);
        return gson.fromJson(strResponse, Weather.class);
    }
}